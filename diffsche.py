# -*- coding: utf-8 -*-
"""
Created on Tue May  3 12:53:30 2016

@author: akira
"""

import sys
import json

org=json.load(open(sys.argv[1]))
tar=json.load(open(sys.argv[2]))
orgs=org['schedule']
tars=tar['schedule']
p=org['place']

odata=dict()
tdata=dict()
for o in orgs:
    key=str(o['dayid'])+"-"+p[str(o['pid'])]['sname']
    if(not (key in odata)):
        odata[key]=[]
    odata[key].append(str(o['time'])+"-"+str(o['name'])+"-"+str(o['genre']))

for o in tars:
    key=str(o['dayid'])+"-"+p[str(o['pid'])]['sname']
    if(not (key in tdata)):
        tdata[key]=[]
    tdata[key].append(str(o['time'])+"-"+str(o['name'])+"-"+str(o['genre']))


for k in odata.keys():
    o=odata[k]
    t=tdata[k]
    for i in range(max(len(o),len(t))):
        if(len(o)<=i):
            print(k,"----",t[i])
            continue
        if(len(t)<=i):
            print(k,o[i],"----")
            continue
        
        if(o[i]!=t[i]):
            print(k,o[i],t[i])
